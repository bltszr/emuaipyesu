# small program to deconstruct MIPS instructions into their respective components
def reg_num_to_name(num: int):
    return_string = "$"
    if num == 0:
        return_string += "zero"
    elif num == 1:
        return_string += "at"
    elif num <= 3:
        return_string += ("v" + str(num - 2))
    elif num <= 7:
        return_string += ("a" + str(num - 4))
    elif num <= 15:
        return_string += ("t" + str(num - 8))
    elif num <= 23:
        return_string += ("s" + str(num - 16))
    elif num <= 25:
        return_string += ("t" + str(num - 16))
    elif num <= 27:
        return_string += ("k" + str(num - 26))
    elif num == 28:
        return_string += "gp"
    elif num == 29:
        return_string += "sp"
    elif num == 30:
        return_string += "fp"
    elif num == 31:
        return_string += "ra"
    return return_string

#reverse of reg_num_to_name
def name_to_reg_num(reg: str):
    try:
        return bin(int(reg[1:]))
    except:
        if reg[1:] == "zero":
            return 0b0
        elif reg[1:] == "at":
            return 0b1
        elif reg[1] == "v":
            return bin(2 + int(reg[2]))
        elif reg[1] == "a":
            return bin(4 + int(reg[2]))
        elif reg[1] == "t" and int(reg[2]) <= 7:
            return bin(8 + int(reg[2]))
        elif reg[1] == "s":
            return bin(16 + int(reg[2]))
        elif reg[1] == "t" and int(reg[2]) > 7:
            return bin(16 + int(reg[2]))
        elif reg[1] == "k":
            return bin(26 + int(reg[2]))

funct_dict = {
    0x20: 'add',
    0x21: 'addu',
    0x24: 'and',
    0x08: 'jr',
    0x27: 'nor',
    0x25: 'or',
    0x2a: 'slt',
    0x2b: 'sltu',
    0x00: 'sll',
    0x02: 'srl',
    0x22: 'sub',
    0x23: 'subu',
    0x1A: 'div',
    0x1B: 'divu',
    0x26: 'xor',
    0x10: 'mfhi',
    0x11: 'mthi',
    0x12: 'mflo',
    0x13: 'mtlo',
    0x18: 'mult',
    0x19: 'multu',
    0x03: 'sra',
}

i_opcode_dict = {
    0x8 : 'addi',
    0x9 : 'addiu',
    0xc : 'andi',
    0x4 : 'beq',
    0x5 : 'bne',
    0x24: 'lbu',
    0x25: 'lhu',
    0x30: 'll',
    0xf : 'lui',
    0x23: 'lw',
    0xd : 'ori',
    0xa : 'slti',
    0xd : 'sltiu',
    0x28: 'sb',
    0x38: 'sc',
    0x29: 'sh',
    0x2b: 'sw',
    0xe: 'xori',
    0x19: 'lh',
    0x06: 'blez',
    0x07: 'bgtz',
    0x20: 'lb',
}

j_opcode_dict = {
    # J-format
    0x2: 'j',
    0x3: 'jal'
}

def sign_ext(immediate: int):
    return immediate
    

class RTypeInstruction:
    def __init__(self, instruction, tobin = None):
        self.tobin = tobin
        if tobin == None:
            if int(instruction[0:6], 2) != 0:
                raise ValueError('Opcode mismatch, type R but given opcode {}.'.format(instruction[0:6]))
            if len(instruction) == 32:
                self.opcode = int(instruction[0:6], 2)
                self.rs = int(instruction[6:11], 2)
                self.rt = int(instruction[11:16], 2)
                self.rd = int(instruction[16:21], 2)
                self.shamt = int(instruction[21:26], 2)
                self.funct = int(instruction[26:32], 2)
            else:
                raise ValueError('Incorrect instruction length.')
        else:
            if len(instruction) != 4:
                raise ValueError('You lack argument, fool!')
            else:
                keys = list(funct_dict.keys())
                values = list(funct_dict.values())

                if instruction[0][0] != "j" and instruction[3][0] == "$":
                    rs = str(name_to_reg_num(instruction[2]))[2:]
                    rt = str(name_to_reg_num(instruction[3]))[2:]
                    rd = str(name_to_reg_num(instruction[1]))[2:]
                    funct = str(bin(keys[values.index(instruction[0])]))[2:]
                    print(i_opcode_dict)

                    self.opcode = "0" * 6
                    self.rs = "0" * (5 - len(rs)) + rs
                    self.rt = "0" * (5 - len(rt)) + rt
                    self.rd = "0" * (5 - len(rd)) + rd
                    self.shamt = "0" * 5
                    self.funct = "0" * (6 - len(funct)) + funct
                elif instruction[3][0] != "$":
                    rt = str(name_to_reg_num(instruction[2]))[2:]
                    rd = str(name_to_reg_num(instruction[1]))[2:]
                    shamt = str(bin(int(instruction[3])))[2:]
                    funct = str(bin(keys[values.index(instruction[0])]))[2:]

                    self.opcode = "0" * 6
                    self.rs = "0" * 5
                    self.rt = "0" * (5 - len(rt)) + rt
                    self.rd = "0" * (5 - len(rd)) + rd
                    self.shamt = "0" * (5 - len(shamt)) + shamt
                    self.funct = "0" * (6 - len(funct)) + funct
                elif instruction[0][0] == "j":
                    rs = str(bin(int(instruction[1])))[2:]
                    funct = str(bin(keys[values.index(instruction[0])]))[2:]

                    self.opcode = "0" * 6
                    self.rs = "0" * (5 - len(rs)) + rs
                    self.rt = "0" * 5
                    self.rd = "0" * 5
                    self.shamt = "0" * 5
                    self.funct = "0" * (6 - len(funct)) + funct
                else:
                    raise ValueError('Incorrect instruction, fool!')

    def __str__(self):
        if(self.tobin == None):
            if funct_dict[self.funct] != 'jr':
                return "{} {}, {}, {}".format(funct_dict[self.funct],
                                            reg_num_to_name(self.rd),
                                            reg_num_to_name(self.rs),
                                            reg_num_to_name(self.rt))
            else:
                return "jr {}".format(reg_num_to_name(self.rs))
        else:
            return self.opcode + self.rs + self.rt + self.rd + self.shamt + self.funct


class ITypeInstruction:
    def __init__(self, instruction, tobin = None):
        self.tobin = tobin
        if tobin == None:
            self.tobin = None
            if int(instruction[0:6], 2) not in i_opcode_dict.keys():
                raise ValueError('Opcode mismatch, type I but given opcode {}.'.format(instruction[0:6]))
            if len(instruction) == 32:
                self.opcode = int(instruction[0:6], 2)
                self.rs = int(instruction[6:11], 2)
                self.rt = int(instruction[11:16], 2)
                self.immediate = int(instruction[16:32], 2)
            else:
                raise ValueError('Incorrect instruction length.')
        else:
            specials = ["lbu", "lhu", "ll", "lui", "lw", "sb", "sc", "sh", "sw", "lh", "lb"]

            if (instruction[0] not in specials and len(instruction) != 4) or (instruction[0] in specials and len(instruction) != 3):
                raise ValueError('You lack argument, fool!')
            else:
                keys = list(i_opcode_dict.keys())
                values = list(i_opcode_dict.values())
                opcode = str(bin(keys[values.index(instruction[0])]))[2:]
                self.opcode = "0" * (6 - len(opcode)) + opcode
                rs = ''
                rt = ''
                imm = 0

                if len(instruction) == 4:
                    rs = str(name_to_reg_num(instruction[1]))[2:]
                    rt = str(name_to_reg_num(instruction[2]))[2:]
                    self.rs = "0" * (5 - len(rs)) + rs
                    self.rt = "0" * (5 - len(rt)) + rt
                    if int(instruction[3]) < 0:
                        imm = str(bin(int(instruction[3]) & 0b1111111111111111))[2:]
                        self.imm = "1" * (16 - len(imm)) + imm
                        self.ext32 = "1" * (32 - len(imm)) + imm
                    else:
                        imm = str(bin(int(instruction[3])))[2:]
                        self.imm = "0" * (16 - len(imm)) + imm
                        self.ext32 = "0" * (32 - len(imm)) + imm
                else:
                    rs = str(name_to_reg_num(instruction[2].split('(')[1][:-1]))[2:]
                    rt = str(name_to_reg_num(instruction[1]))[2:]
                    self.rs = "0" * (5 - len(rs)) + rs
                    self.rt = "0" * (5 - len(rt)) + rt
                    if int(instruction[2].split('(')[0]) < 0:
                        imm = str(bin(int(instruction[2].split('(')[0]) & 0b1111111111111111))[2:]
                        self.imm = "1" * (16 - len(imm)) + imm
                        self.ext32 = "1" * (32 - len(imm)) + imm
                    else:
                        imm = str(bin(int(instruction[2].split('(')[0])))[2:]
                        self.imm = "0" * (16 - len(imm)) + imm
                        self.ext32 = "0" * (32 - len(imm)) + imm
            
    def __str__(self):
        if self.tobin == None:
            # if it's some kind of immediate or branching instruction
            opc: str = i_opcode_dict[self.opcode]
            if (("i" in opc) or (opc in ("beq", "bne"))):
                return "{} {}, {}, {}".format(opc,
                                            reg_num_to_name(self.rs),
                                            reg_num_to_name(self.rt),
                                            self.immediate)
            # then it must be some kind of memory thing
            else:
                return "{} {}, {}({})".format(opc,
                                            reg_num_to_name(self.rs),
                                            self.immediate,
                                            reg_num_to_name(self.rt))
        else:
            return self.opcode + self.rs + self.rt + self.imm
            

class JTypeInstruction:
    def __init__(self, instruction, tobin = None):
        self.tobin = tobin
        if tobin == None:
            if int(instruction[0:6], 2) not in j_opcode_dict.keys():
                raise ValueError('Opcode mismatch, type J but given opcode {}.'.format(instruction[0:6]))
            if len(instruction) == 32:
                self.opcode = int(instruction[0:6], 2)
                self.address = int(instruction[6:32], 2)
            else:
                raise ValueError('Incorrect instruction length.')
        else:
            if len(instruction) != 2:
                raise ValueError('You lack argument, fool!')
            else:
                keys = list(j_opcode_dict.keys())
                values = list(j_opcode_dict.values())
                opcode = str(bin(int(str(keys[values.index(instruction[0])]), 16)))[2:]
                self.opcode = "0" * (6 - len(opcode)) + opcode
                address = str(bin(int(instruction[1])))[2:]
                self.address = ("0" * (32 - len(address)) + address)[4:-2]

    def __str__(self):
        if tobin == None:
            return "{} {}".format(j_opcode_dict[self.opcode], self.address)
        else:
            return self.opcode + self.address

# I realized soon enough that this is not as simple as I had thought

def decide_type(instruction: str):
    format_type: str = instruction[0:2]
    instruction = instruction[2:]
    if format_type == '0b':
        pass
    elif format_type == '0x':
        instruction = "{0:032b}".format(int(instruction, 16))
    elif format_type == '0o':
        instruction = "{0:032b}".format(int(instruction, 8))
    else:
        raise ValueError('No valid prefix.')
    opcode: str = instruction[0:6]
    if opcode == '000000':
        rs = "rs: " + instruction[6:11]
        rt = "rt: " + instruction[11:16]
        rd = "rd: " + instruction[16:21]
        shamt = "shamt: " + instruction[21:26]
        funct = "funct: " + instruction[26:6]

        return "opcode: " + opcode, rs, rt, rd, shamt, funct, "===================", RTypeInstruction(instruction)
    elif (opcode == '000010') or (opcode == '000011'):
        pseudo = "pseudo-address: " + instruction[6:]
        return "opcode: " + opcode, pseudo, "===================", JTypeInstruction(instruction)
    else:
        rs = "rs: " + instruction[6:11]
        rt = "rt: " + instruction[11:16]
        imm = "imm: " + instruction[16:21]

        return "opcode: " + opcode, rs, rt, imm, "===================", ITypeInstruction(instruction)

#Decide_type equivalent for to binary
def tobin(instruction):
    if instruction[0] in funct_dict.values():
        R = RTypeInstruction(instruction, "tobin")
        return "opcode: " + R.opcode, "rs: " + R.rs, "rt: " + R.rt, "rd: " + R.rd, "shamt: " + R.shamt, "funct: " + R.funct, "===================", R
    elif instruction[0] in i_opcode_dict.values():
        if len(instruction) == 4:
            if "x" not in instruction[3]:
                instruction[3] = int(instruction[3])
            else: 
                instruction[3] = int(instruction[3][2:], 16)
        I = ITypeInstruction(instruction, "tobin")
        return "opcode: " + I.opcode, "rs: " + I.rs, "rt: " + I.rt, "imm: " + I.imm, "ext32: " + I.ext32, "===================", I   
    elif instruction[0] in j_opcode_dict.values():
        J = JTypeInstruction(instruction, "tobin")
        return "opcode: " + J.opcode, "address: " + J.address, "===================", J

def main():
    print("Welcome! Type in MIPS instructions in binary, hex, or octal form, with prefixes. Type 'exit' to exit.")
    while True:
        input_string = input("$> ")
        if input_string[0:2] == '0x' or input_string[0] == '0b' or input_string[0] == '0o':
            try:
                command = decide_type(input_string)
                for isi in command:
                    print(isi)
            except Exception as e:
                print(e)

        #add feature translate from mips code to binary
        elif input_string[0:5] == "tobin": #write "tobin" first before input
            command = input_string[6:].split(", ")
            firsts = command[0].split()
            command[0] = firsts[1]
            command.insert(0, firsts[0])

            result = tobin(command)
            for isi in result:
                print(isi)
        
        #add feature get register from number
        elif input_string[0:6] == "getreg": #write "getreg" first before input
            if input_string[7:9] == "0b":
                print(reg_num_to_name(int(input_string[10:], 2)))
            elif input_string[7:9] == "0x":
                print(reg_num_to_name(int(input_string[10:], 16)))
            elif input_string[7:9] == "0o":
                print(reg_num_to_name(int(input_string[10:], 8)))
            else:
                print(reg_num_to_name(int(input_string[7:])))
        elif input_string == 'exit':
            print("Goodbye!")
            break
        
    
if __name__ == "__main__":
    main()